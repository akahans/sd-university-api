<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name' => 'Course ' . $faker->word,
        'capacity' => $faker->numberBetween($min = 3, $max = 8),
    ];
});
