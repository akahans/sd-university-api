<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;


class CourseController extends Controller
{
    /**
     * Return Courses List with availability check.
     *
     * @return List of courses
     */
    public function getCourses(){

    	$courses = Course::all();

    	$coursesSummary = array();

    	foreach($courses as $course){
    		$registeredUsers = $course->users()->count();
    		$availability = $course->capacity > $registeredUsers;

    		array_push( $coursesSummary, array(
    			'name'=> $course->name,
    			'course_id'=> $course->id,
    			'registrations'=> $registeredUsers.'/'.$course->capacity,
    			'available' => $availability
    		));

    	}

        return json_encode(array(
        	'university_courses' => $coursesSummary
		), JSON_FORCE_OBJECT);
    }
}
